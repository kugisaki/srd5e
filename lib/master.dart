import 'package:flutter/material.dart';

import 'account.dart';
import 'contents.dart';

final redTheme = Colors.red[300];
final redThemeLighter = Colors.redAccent[100];
final offwhiteTheme = Colors.grey[50];

class Master extends StatelessWidget {
  Master({
    this.child,
    this.title,
  });

  final Widget child;
  final String title;

  final GlobalKey<ScaffoldState> _scaffold = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffold,
      appBar: new AppBar(
        title: new Text(
          title,
          textAlign: TextAlign.center,
          style: new TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w300,
            fontSize: 18.0,
          )
        ),
        elevation: 0.1,
        brightness: Brightness.light,
        automaticallyImplyLeading: false,
        centerTitle: false,
        backgroundColor: offwhiteTheme,
        leading: new IconButton(
          icon: new ImageIcon(
            new AssetImage('graphics/tomes.png'),
          ),
          color: redTheme,
          onPressed: () => _scaffold.currentState.openDrawer(),
        ),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.search),
            color: Colors.black,
            onPressed: () => {

            },
          ),
          new IconButton(
            icon: new Icon(Icons.more_vert),
            color: Colors.black, 
            onPressed: () => _scaffold.currentState.openEndDrawer(),
          ),
        ],
      ),
      endDrawer: new UserDrawer(),
      drawer: new ContentsDrawer(),
      body: new Container(
        decoration: new BoxDecoration(
          border: new Border(top: new BorderSide(color: Theme.of(context).dividerColor))
        ),
        child: child
      ), 
    );
  }
}