import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';

import 'classes.dart';

final redTheme = Colors.red[300];
final redThemeLighter = Colors.redAccent[100];
final offwhiteTheme = Colors.grey[50];

final reference = FirebaseDatabase.instance.reference().child('contents');

final menuTextStyle = new TextStyle(
  fontWeight: FontWeight.w300,
  fontSize: 18.0,
);

final menuTextStyleW = new TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.w500,
  fontSize: 16.0,
);

class ContentsDrawer extends StatefulWidget {
  @override
  _ContentsDrawerState createState() => new _ContentsDrawerState();
}

class _ContentsDrawerState extends State<ContentsDrawer> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: new Container(
        color: redTheme,
        child: new Column(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                border: new Border(
                  bottom: new BorderSide(width: 1.0, color: redThemeLighter),
                ),
              ),
              padding: new EdgeInsets.fromLTRB(8.0, 44.0, 8.0, 8.0),
              child: new ListTile(
                title: new Text(
                      'Glossary', 
                      style: menuTextStyleW,
                    ),
                dense: true,
                onTap: () { Navigator.popAndPushNamed(context, '/'); },
              ),
            ),
            new  Flexible(
              child: new FirebaseAnimatedList(
                query: reference,
                sort: (a, b) => a.key.compareTo(b.key),
                padding: new EdgeInsets.all(8.0),
                reverse: false,
                itemBuilder: (_, DataSnapshot snapshot, Animation<double> animation) {

                  var type = snapshot.value['name'].toString().toLowerCase();
                  var options = snapshot.value['options'];

                  if (options == null) {
                    return new ListTile(
                      title: new Text(
                        snapshot.value['name'], 
                        style: menuTextStyleW
                      ),
                      onTap: () {},
                    ); 
                  } else {                
                    var subItems = new List<Widget>.generate(options.length, (int index) {
                      return new ListTile( 
                        title: new Text(
                          options[index]['name'],
                          style: new TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w300,
                            fontSize: 14.0,
                          ),
                        ),
                        onTap: () { 
                          Navigator.push(context, new MaterialPageRoute(
                            builder: (BuildContext context) => new Classes(name: options[index]['name'], type: type,)),
                          );
                        }
                      );
                    });

                    return new ExpansionTile(
                      key: new PageStorageKey<DataSnapshot>(snapshot),
                      initiallyExpanded: false,
                      title: new Text(
                        snapshot.value['name'], 
                        style: menuTextStyleW
                      ),
                      children: subItems,
                    );
                  }
                },
              ),
            ),            
          ],
        ), 
      ),
    );
  }
}