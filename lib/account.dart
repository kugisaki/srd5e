import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'dart:async';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';

final auth = FirebaseAuth.instance;

final redTheme = Colors.red[300];
final redThemeLighter = Colors.redAccent[100];
final offwhiteTheme = Colors.grey[50];

final googleSignIn = new GoogleSignIn();
final analytics = new FirebaseAnalytics();

Future<bool> _ensureLoggedIn() async {
  GoogleSignInAccount user = googleSignIn.currentUser;
  if (user == null)
    user = await googleSignIn.signInSilently();
  if (user == null) {
    await googleSignIn.signIn();
    analytics.logLogin();
  }
  if (await auth.currentUser() == null) {
    GoogleSignInAuthentication credentials = await googleSignIn.currentUser.authentication;
    await auth.signInWithGoogle(
      idToken: credentials.idToken,
      accessToken: credentials.accessToken,
    );
  }   
  return user != null;
}

final menuTextStyle = new TextStyle(
  fontWeight: FontWeight.w300,
  fontSize: 16.0,
);

final menuTextStyleW = new TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.w300,
  fontSize: 18.0,
);

class UserDrawer extends StatefulWidget {
  @override
  _UserDrawerState createState() => new _UserDrawerState();
}

class _UserDrawerState extends State<UserDrawer> {
  bool loggedIn = googleSignIn.currentUser != null;

  void _showAccount() {
    _ensureLoggedIn().then((logged) {
      setState(() {
        loggedIn = logged;
      });
    });
  }

  void _logOut() {
    googleSignIn.signOut();
    analytics.logEvent(name: 'sign_out');

    setState(() {
      loggedIn = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!loggedIn) {
      return new Drawer(
        child: new ListView(
          children: <Widget>[
            new Container(
              margin: const EdgeInsets.fromLTRB(40.0, 100.0, 40.0, 40.0),
              child: new RaisedButton(
                color: redTheme,
                child: const Text(
                  'Log In', 
                  style: const TextStyle(
                    color: Colors.white,
                  ),
                ),
                onPressed: () { _showAccount(); },
              ),
            )
          ],
        ),
      );
    } else {
      return new Drawer(
        child: new ListView(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                border: new Border(
                  bottom: new BorderSide(width: 1.0, color: Theme.of(context).dividerColor),
                ),
              ),         
              padding: new EdgeInsets.all(8.0),
              child: new ListTile(
                leading: new CircleAvatar(
                  backgroundImage: new NetworkImage(googleSignIn.currentUser.photoUrl),
                  backgroundColor: Colors.blueGrey,
                ),
                title: new Text(
                      googleSignIn.currentUser.displayName, 
                      style: menuTextStyle,
                    ),
                subtitle: new Text(
                    googleSignIn.currentUser.email, 
                    style: menuTextStyle,
                  ),
                onTap: () {},
              ),
            ),
            // new ListTile(
            //   title: new Text(
            //       'Reference',
            //       style: menuTextStyle,
            //     ),
            //   onTap: () { Navigator.popAndPushNamed(context, '/'); },
            // ),
            new ListTile(
              title: new Text(
                  'Characters',
                  style: menuTextStyle,
                ),
              onTap: () { Navigator.popAndPushNamed(context, '/characters'); },
            ),
            new ListTile(
              title: new Text(
                  'Log out',
                  style: menuTextStyle,
                ),
              onTap: () { _logOut(); },
            ),
          ],
        )
      );
    }
  }
}